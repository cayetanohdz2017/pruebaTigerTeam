package com.prueba.tigerTeam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.tigerTeam.entity.RespuestaExamen;

@Repository
public interface RespuestaExamenRepository extends JpaRepository<RespuestaExamen, Long> {
	
	public List<RespuestaExamen> findByAsignacionExamen_Estudiante_IdAndAsignacionExamen_Examen_Id(Long idEstudiante, Long idExamen);

}
