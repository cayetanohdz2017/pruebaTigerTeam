package com.prueba.tigerTeam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.tigerTeam.entity.AsignacionExamen;

@Repository
public interface AsignacionExamenRepository extends JpaRepository<AsignacionExamen, Long>{

}
