package com.prueba.tigerTeam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.tigerTeam.entity.Examen;

@Repository
public interface ExamenRepository extends JpaRepository<Examen, Long> {

}
