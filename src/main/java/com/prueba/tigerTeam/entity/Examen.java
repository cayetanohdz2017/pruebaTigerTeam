package com.prueba.tigerTeam.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "examen")
public class Examen {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column(name = "nombre")
	private String nombre;
	
	@OneToMany(mappedBy = "examen", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PreguntaExamen> pregunta;
	
	@Column(name = "totalScore")
	private Integer totalScore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	@JsonManagedReference
	public List<PreguntaExamen> getPregunta() {
		return pregunta;
	}

	public void setPregunta(List<PreguntaExamen> pregunta) {
		this.pregunta = pregunta;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	
	

}
