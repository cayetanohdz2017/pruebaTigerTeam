package com.prueba.tigerTeam.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "preguntas")
public class PreguntaExamen {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String texto;
	
	@OneToMany(mappedBy = "preguntaExamen", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Respuesta> respuesta;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_examen")
	private Examen examen;
	
	private Integer score;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	@JsonManagedReference
	public List<Respuesta> getRespuestaExamen() {
		return respuesta;
	}

	public void setRespuestaExamen(List<Respuesta> respuestaExamen) {
		this.respuesta = respuestaExamen;
	}

	
	@JsonBackReference
	public Examen getExam() {
		return examen;
	}

	public void setExam(Examen examen) {
		this.examen = examen;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
	
	

}
