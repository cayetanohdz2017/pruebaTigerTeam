package com.prueba.tigerTeam.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "respuesta_examen")
public class RespuestaExamen {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_asignacion_examen")
	private AsignacionExamen asignacionExamen;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pregunta_examen")
	private PreguntaExamen preguntaExamen;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_respuesta")
	private Respuesta respuesta;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public AsignacionExamen getAsignacionExamen() {
		return asignacionExamen;
	}


	public void setAsignacionExamen(AsignacionExamen asignacionExamen) {
		this.asignacionExamen = asignacionExamen;
	}


	public PreguntaExamen getPreguntaExamen() {
		return preguntaExamen;
	}


	public void setPreguntaExamen(PreguntaExamen preguntaExamen) {
		this.preguntaExamen = preguntaExamen;
	}


	public Respuesta getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(Respuesta respuesta) {
		this.respuesta = respuesta;
	}
	
	

}
