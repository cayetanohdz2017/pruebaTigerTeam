package com.prueba.tigerTeam.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tigerTeam.entity.AsignacionExamen;
import com.prueba.tigerTeam.entity.Examen;
import com.prueba.tigerTeam.iService.ExamenService;

@RestController
@RequestMapping("/api/examen")
public class ExamenController {
	
	
	@Autowired
	private ExamenService examenService ;
	
	
	@GetMapping("/obtener")
	public ResponseEntity<?> obtenerTodosLosExamenes(){
		
		List<Examen> examen = examenService.obtenerTodosLosExamenes();
		
		return ResponseEntity.ok().body(examen);
	}
	
	@GetMapping("/obtener/{id}")
	public ResponseEntity<?> obtenerExamenPorId(@PathVariable Long id){
		
		Optional<Examen> oExamen = examenService.obtenerExamenPorId(id);
		
		if(!oExamen.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		
		return ResponseEntity.ok().body(oExamen.get());
	}
	
	@PostMapping("/crear")
	public ResponseEntity<?> crearExamen(@RequestBody Examen examen){
		
		Examen nuevoExamen = examenService.crearExamen(examen);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoExamen);
	}
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminarExamenPorId(@PathVariable Long id){
		
		if(!examenService.obtenerExamenPorId(id).isPresent()) {
			
			return ResponseEntity.notFound().build();
			
		}
		
		examenService.eliminarExamenPorId(id);
		
		return ResponseEntity.ok().build();
		
	}
	
//	@PostMapping("/asignar-examen/{id_examen}/estudiantes/{id_estudiante}")
//	public ResponseEntity<?> asignarExamenAEstudiante(@PathVariable Long id_examen, @PathVariable Long id_estudiante,
//			@RequestBody AsignacionExamen asignacionExamen){
//		
//		
//		if(!examenService.obtenerExamenPorId(id_examen).isPresent()) {
//			return ResponseEntity.notFound().build();
//		}
//		
//	
//		
//		examenService.asignarExamenAEstudiante(id_examen, id_estudiante, asignacionExamen.getFechaExamen());
//		
//		return ResponseEntity.ok().build();
//		
//	}

}
