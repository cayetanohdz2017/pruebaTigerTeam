package com.prueba.tigerTeam.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tigerTeam.entity.Estudiante;
import com.prueba.tigerTeam.iService.EstudianteService;

@RestController
@RequestMapping("/api/estudiantes")
public class EstudiantesController {
	
	
	@Autowired
	private EstudianteService estudianteService;
	
	@GetMapping("/obtener")
	public ResponseEntity<?> obtenerTodosLosEstudiantes(){
		
		List<Estudiante> estudiante = estudianteService.obtenerTodosLosEstudiantes();
		
		return ResponseEntity.ok().body(estudiante);
	}
	
	@GetMapping("/obtener/{id}")
	public ResponseEntity<?> obtenerEstudiantePorId(@PathVariable Long id){
		
		Optional<Estudiante> oEstudiante = estudianteService.obtenerEstudiantePorId(id);
		
		if(!oEstudiante.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(oEstudiante.get());
	}
	
	@PostMapping("/registrar")
	public ResponseEntity<?> crearNuevoEstudiante(@RequestBody Estudiante estudiante){
		
		Estudiante nuevoEstudiante = estudianteService.crearEstudiante(estudiante);
		
		System.out.println("estudiante: " + nuevoEstudiante);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevoEstudiante);
	}
	
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminarEstudiantePorId(@PathVariable Long id){
		
		if(!estudianteService.obtenerEstudiantePorId(id).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		estudianteService.eliminarEstudiantePorId(id);
		
		return ResponseEntity.ok().build();
	}
	

}
