package com.prueba.tigerTeam.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tigerTeam.entity.RespuestaExamen;
import com.prueba.tigerTeam.iService.RespuestaExamenService;

@RestController
@RequestMapping("/api/respuesta-examen")
public class RespuestaExamenController {
	
	@Autowired
	private RespuestaExamenService respuestaExamenService;
	
	
	@PostMapping("/registrar-respuesta")
	public ResponseEntity<?> crearRespuestaExamen(@RequestParam Long id_asignacion_examen,
			@RequestParam Long id_pregunta_examen, @RequestParam Long id_respuesta){
		
		RespuestaExamen nuevaRespuestaExamen = respuestaExamenService.crearRespuestaExamen(id_asignacion_examen, id_pregunta_examen, id_respuesta);
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevaRespuestaExamen);
	}
	
	@GetMapping("/obtener")
	public ResponseEntity<?> obtenerTodasLasRespuestaExamen(){
		
		List<RespuestaExamen> respuestaExamen = respuestaExamenService.obtenerTodasLasRespuestasDeExamen();
		
		return ResponseEntity.ok().body(respuestaExamen);
	}
	
	@GetMapping("/obtener/{id}")
	public ResponseEntity<?> obtenerRespuestaExamenPorId(@PathVariable Long id){
		
		Optional<RespuestaExamen> oRespuestaExamen = respuestaExamenService.obtenerRespuestaExamenPorId(id);
		
		if(!oRespuestaExamen.isPresent()) {
			
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(oRespuestaExamen.get());
	}
	
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizarRespuestaExamen(@PathVariable Long id, @RequestBody RespuestaExamen respuestaExamen){
		
		Optional<RespuestaExamen> respuestaExamenActualizar = respuestaExamenService.obtenerRespuestaExamenPorId(id);
		
		if(!respuestaExamenActualizar.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		respuestaExamenActualizar.get().setAsignacionExamen(respuestaExamen.getAsignacionExamen());
		respuestaExamenActualizar.get().setPreguntaExamen(respuestaExamen.getPreguntaExamen());
		respuestaExamenActualizar.get().setRespuesta(respuestaExamen.getRespuesta());
		
		respuestaExamenService.actualizarRespuestaExamen(respuestaExamenActualizar.get());
		
		return ResponseEntity.ok().body(respuestaExamenActualizar.get());
	}
	
	@DeleteMapping("/eliminar{id}")
	public ResponseEntity<?> eliminarRespuestaExamenPorId(@PathVariable Long id){
		
		Optional<RespuestaExamen> respuestaExamenEliminar = respuestaExamenService.obtenerRespuestaExamenPorId(id);
		
		if(!respuestaExamenEliminar.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		respuestaExamenService.eliminarRespuestaExamen(id);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/respuesta-examen-estudiante/{id_estudiante}/examen/{id_examen}")
	public ResponseEntity<?> obtenerRespuestaExamenPorEstudianteyMateria(@PathVariable Long id_estudiante,
			@PathVariable Long id_examen){
		
		List<RespuestaExamen> respuestaExamenPorEstudiante= respuestaExamenService.obtenerRespuestaExamenPorEstudianteyMateria(id_estudiante, id_examen);
		return ResponseEntity.ok().body(respuestaExamenPorEstudiante);
	}
	
	@GetMapping("/calificacion-examen-estudiante/{id_estudiante}/examen/{id_examen}")
	public ResponseEntity<?> obtenerCalificacionExamenPorEstudianteyMateria(@PathVariable Long id_estudiante,
			@PathVariable Long id_examen){
		
		List<Map.Entry<String, Object>> respuestaExamenPorEstudiante= respuestaExamenService.obtenerCalificacionPorEstudiantePorMateria(id_estudiante, id_examen);
		return ResponseEntity.ok().body(respuestaExamenPorEstudiante);
	}
}
