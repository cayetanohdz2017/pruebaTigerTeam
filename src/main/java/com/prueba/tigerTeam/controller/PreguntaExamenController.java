package com.prueba.tigerTeam.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tigerTeam.entity.Examen;
import com.prueba.tigerTeam.entity.PreguntaExamen;
import com.prueba.tigerTeam.entity.Respuesta;
import com.prueba.tigerTeam.iService.ExamenService;
import com.prueba.tigerTeam.iService.PreguntaService;

@RestController
@RequestMapping("/api/preguntas-examen")
public class PreguntaExamenController {
	
	
	@Autowired
	private PreguntaService preguntaService;
	
	@Autowired
	private ExamenService examenService;
	
	@GetMapping("/obtener")
	public ResponseEntity<?> obtenerTodasLasPreguntas(){
		
		List<PreguntaExamen> preguntaExamen= preguntaService.obtenerTodasLasPreguntasDeExamenes();
		
		return ResponseEntity.ok().body(preguntaExamen);
	}
	
	@GetMapping("/obtener/{id}")
	public ResponseEntity<?> obtenerPreguntasPorId(@PathVariable Long id){
		
		Optional<PreguntaExamen> oPreguntaExamen = preguntaService.obtenerPreguntaExamenPorId(id);
		
		if(!oPreguntaExamen.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		
		return ResponseEntity.ok().body(oPreguntaExamen.get());
	}
	
	
	@PostMapping("/registrar-pregunta/{id_examen}")
	public ResponseEntity<?> crearPreguntaExamen(@PathVariable Long id_examen, @RequestBody PreguntaExamen preguntaExamen){
		
		Examen examen = examenService.obtenerExamenPorId(id_examen).orElse(null);
	    if(examen == null) {
	        return ResponseEntity.notFound().build();
	    }
	    preguntaExamen.setExam(examen);
		
		PreguntaExamen preguntaExamenNuevo = preguntaService.crearPreguntaExamen(id_examen, preguntaExamen.getTexto(), preguntaExamen.getScore());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(preguntaExamenNuevo);
	}

	
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminarPreguntaExamenPorId(@PathVariable Long id){
		
		Optional<PreguntaExamen> preguntaExamenDelete = preguntaService.obtenerPreguntaExamenPorId(id);
		
		if(!preguntaExamenDelete.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		preguntaService.eliminarPreguntarExamen(id);
		
		return ResponseEntity.ok().build();
	}
	
	

}
