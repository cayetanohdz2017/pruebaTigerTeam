package com.prueba.tigerTeam.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tigerTeam.entity.Respuesta;
import com.prueba.tigerTeam.iService.RespuestaService;

@RestController
@RequestMapping("/api/respuesta")
public class RespuestaController {
	
	@Autowired
	private RespuestaService respuestaService;
	
	
	@PostMapping("/registrar-respuesta")
	public ResponseEntity<?> crearRespuesta(@RequestBody Respuesta respuesta){
		
		Respuesta nuevaRespuesta = respuestaService.crearRespuesta(respuesta.getTexto(), respuesta.isCorrecto(), respuesta.getPreguntaExamen().getId());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevaRespuesta);
	}
	
	@GetMapping("/obtener")
	public ResponseEntity<?> obtenerTodasLasRespuestas(){
		
		List<Respuesta> respuestas = respuestaService.obtenerTodasLasResppuestas();
		
		return ResponseEntity.ok().body(respuestas);
	}
	
	@GetMapping("/obtener/{id}")
	public ResponseEntity<?> obtenerRespuestaPorId(@PathVariable Long id){
		
		Optional<Respuesta> oRespuesta = respuestaService.obtenerRespuestaPorId(id);
		
		if(!oRespuesta.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(oRespuesta.get());
	}
	
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizarRespuesta(@PathVariable Long id, @RequestBody Respuesta respuesta){
		Optional<Respuesta> oRespuesta = respuestaService.obtenerRespuestaPorId(id);
		
		if(!oRespuesta.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oRespuesta.get().setTexto(respuesta.getTexto());
		oRespuesta.get().setCorrecto(respuesta.isCorrecto());
		oRespuesta.get().setPreguntaExamen(respuesta.getPreguntaExamen());
		
		respuestaService.actualizarRespuesta(oRespuesta.get());
		
		return ResponseEntity.ok().body(oRespuesta.get());
	}
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminarRespuestaPorId(@PathVariable Long id){
		
		Optional<Respuesta> oRespuesta = respuestaService.obtenerRespuestaPorId(id);
		
		if(!oRespuesta.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		respuestaService.eliminarRespuesta(id);
		
		
		return ResponseEntity.ok().build();
	}

}
