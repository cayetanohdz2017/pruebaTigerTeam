package com.prueba.tigerTeam.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tigerTeam.entity.AsignacionExamen;
import com.prueba.tigerTeam.iService.AsignacionExamenService;

@RestController
@RequestMapping("/api/asignacion-examen")
public class AsignacionExamenController {

	@Autowired
	private AsignacionExamenService asignacionExamenService;
	
	
	@PostMapping("/registrar-asignacion-examen/{id_examen}/estudiante/{id_estudiante}")
	public ResponseEntity<?> crearAsignacionExamen(@PathVariable Long id_examen, @PathVariable Long id_estudiante, 
			@RequestBody AsignacionExamen asignacionExamen){
		
		AsignacionExamen nuevaAsignacionExamen = asignacionExamenService.crearAsignacionExamen(
				id_examen, 
				id_estudiante,
				asignacionExamen.getFechaExamen());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(nuevaAsignacionExamen);
	}
	
	@GetMapping("/obtener")
	public ResponseEntity<?> obtenerTodosLosExamenesAsignados(){
		
		List<AsignacionExamen> asignacionExamenes = asignacionExamenService.obtenerTodosLosExamenesAsignados();
		
		return ResponseEntity.ok().body(asignacionExamenes);
	}
	
	@GetMapping("/obtener/{id}")
	public ResponseEntity<?> obtenerExamenesAsignadosPorId(@PathVariable Long id){
		
		Optional<AsignacionExamen> asignacionExamenId = asignacionExamenService.obtenerExamenesAsignadorPorId(id);
		
		if(!asignacionExamenId.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(asignacionExamenId.get());
	}
	
	@GetMapping("/obtener/examen/{id_examen}")
	public ResponseEntity<?> obtenerExamenesAsignadosPorExamen(@PathVariable Long id_examen){
		
		
		Optional<AsignacionExamen> asignacionExamenPorExamen = asignacionExamenService.obtenerExamenesAsignadorPorExamen(id_examen);
		
		if(!asignacionExamenPorExamen.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(asignacionExamenPorExamen.get());
	}
	
	@GetMapping("/obtener/estudiante/{id_estudiante}")
	public ResponseEntity<?> obtenerExamenesAsignadosPorEstudiante(@PathVariable Long id_estudiante){
		
		Optional<AsignacionExamen> asignacionExamenPorEstudiante = asignacionExamenService.obtenerExamenesAsignadorPorEstudiante(id_estudiante);
		
		if(!asignacionExamenPorEstudiante.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(asignacionExamenPorEstudiante.get());
	}
	
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizarAsignacionExamen(@PathVariable Long id, @RequestBody AsignacionExamen asignacionExamen){
		
		Optional<AsignacionExamen> asignacionExamenActualizar = asignacionExamenService.obtenerExamenesAsignadorPorId(id);
		
		if(!asignacionExamenActualizar.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		asignacionExamenActualizar.get().setEstudiante(asignacionExamen.getEstudiante());
		asignacionExamenActualizar.get().setExamen(asignacionExamen.getExamen());
		asignacionExamenActualizar.get().setFechaExamen(asignacionExamen.getFechaExamen());
		
		asignacionExamenService.actualizarAsignacionExamen(asignacionExamenActualizar.get());
		
		return ResponseEntity.ok().body(asignacionExamenActualizar.get());
	}
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminarExamenAsignadoPorId(@PathVariable Long id){
		
		Optional<AsignacionExamen> aeEliminar = asignacionExamenService.obtenerExamenesAsignadorPorId(id);
		
		if(!aeEliminar.isPresent()) {
			ResponseEntity.notFound().build();
		}
		
		asignacionExamenService.eliminarAsignacionExamen(id);
	
		return ResponseEntity.ok().build();
	}
	
	
}
