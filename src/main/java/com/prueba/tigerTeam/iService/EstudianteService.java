package com.prueba.tigerTeam.iService;

import java.util.List;
import java.util.Optional;

import com.prueba.tigerTeam.entity.Estudiante;

public interface EstudianteService {
	
	public Estudiante crearEstudiante(Estudiante estudiante);
	
	public List<Estudiante> obtenerTodosLosEstudiantes();
	
	public Optional<Estudiante> obtenerEstudiantePorId(Long id);
	
	public void eliminarEstudiantePorId(Long id);

}
