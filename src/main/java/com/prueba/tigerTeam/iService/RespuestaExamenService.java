package com.prueba.tigerTeam.iService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.prueba.tigerTeam.entity.RespuestaExamen;

public interface RespuestaExamenService {
	
	public RespuestaExamen crearRespuestaExamen(Long id_asignacion_examen, Long id_pregunta_examen, Long id_respuesta);
	
	public List<RespuestaExamen> obtenerTodasLasRespuestasDeExamen();
	
	public Optional<RespuestaExamen> obtenerRespuestaExamenPorId(Long id);
	
	public void actualizarRespuestaExamen(RespuestaExamen respuestaExamen);
	
	public void eliminarRespuestaExamen(Long id);
	
	public List<RespuestaExamen> obtenerRespuestaExamenPorEstudianteyMateria(Long id_estudiante, Long id_examen);
	
	public List<Map.Entry<String, Object>> obtenerCalificacionPorEstudiantePorMateria(Long id_estudiante, Long id_examen);

}
