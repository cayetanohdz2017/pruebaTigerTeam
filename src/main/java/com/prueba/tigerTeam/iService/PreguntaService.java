package com.prueba.tigerTeam.iService;

import java.util.List;
import java.util.Optional;

import com.prueba.tigerTeam.entity.PreguntaExamen;
import com.prueba.tigerTeam.entity.Respuesta;

public interface PreguntaService {
	
	public PreguntaExamen crearPreguntaExamen(Long id_examen, String texto, Integer score);
	
	public List<PreguntaExamen> obtenerTodasLasPreguntasDeExamenes();
	
	public Optional<PreguntaExamen> obtenerPreguntaExamenPorId(Long id);
	
	
	
	public void eliminarPreguntarExamen(Long id);

}
