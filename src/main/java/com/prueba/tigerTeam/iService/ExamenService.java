package com.prueba.tigerTeam.iService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.prueba.tigerTeam.entity.Examen;

public interface ExamenService {
	
	public Examen crearExamen(Examen examen);
	
	public List<Examen> obtenerTodosLosExamenes();
	
	public Optional<Examen> obtenerExamenPorId(Long id);
	
	public void eliminarExamenPorId(Long id);
	
	public void asignarExamenAEstudiante(Long id_examen, Long id_estudiante, LocalDateTime fechaPresentacion);

}
