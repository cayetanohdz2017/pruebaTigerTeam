package com.prueba.tigerTeam.iService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.prueba.tigerTeam.entity.AsignacionExamen;

public interface AsignacionExamenService {
	
	public AsignacionExamen crearAsignacionExamen(Long id_examen, Long id_estudiante, LocalDateTime fechaExamen);
	
	public List<AsignacionExamen> obtenerTodosLosExamenesAsignados();
	
	public Optional<AsignacionExamen> obtenerExamenesAsignadorPorEstudiante(Long id_estudiante);
	
	public Optional<AsignacionExamen> obtenerExamenesAsignadorPorExamen(Long id_examen);
	
	public Optional<AsignacionExamen> obtenerExamenesAsignadorPorId(Long id);
	
	public void actualizarAsignacionExamen(AsignacionExamen asignacionExamen);
	
	public void eliminarAsignacionExamen(Long id);

}
