package com.prueba.tigerTeam.iService;

import java.util.List;
import java.util.Optional;

import com.prueba.tigerTeam.entity.Respuesta;

public interface RespuestaService {
	
	public Respuesta crearRespuesta(String texto, boolean correcto, Long id_pregunta_examen);
	
	public List<Respuesta> obtenerTodasLasResppuestas();
	
	public Optional<Respuesta> obtenerRespuestaPorId(Long id);
	
	public void actualizarRespuesta(Respuesta respuesta);
	
	public void eliminarRespuesta(Long id);

}
