package com.prueba.tigerTeam.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tigerTeam.entity.PreguntaExamen;
import com.prueba.tigerTeam.entity.Respuesta;
import com.prueba.tigerTeam.iService.RespuestaService;
import com.prueba.tigerTeam.repository.PreguntaExamenRepository;
import com.prueba.tigerTeam.repository.RespuestaRepository;

@Service
public class RespuestaServiceImpl implements RespuestaService {

	@Autowired
	private RespuestaRepository respuestaRepository;
	
	@Autowired
	private PreguntaExamenRepository preguntaExamenRepository;
	
	
	@Override
	public Respuesta crearRespuesta(String texto, boolean correcto, Long id_pregunta_examen) {

		PreguntaExamen preguntaExamen = preguntaExamenRepository.findById(id_pregunta_examen)
				.orElseThrow(() -> new IllegalArgumentException("No se encontro la pregunta de examen con id: " + id_pregunta_examen));
		Respuesta respuesta = new Respuesta();
		respuesta.setTexto(texto);
		respuesta.setCorrecto(correcto);
		respuesta.setPreguntaExamen(preguntaExamen);
		
		return respuestaRepository.save(respuesta);
	}

	@Override
	public List<Respuesta> obtenerTodasLasResppuestas() {
		
		return respuestaRepository.findAll();
	}

	@Override
	public Optional<Respuesta> obtenerRespuestaPorId(Long id) {

		return respuestaRepository.findById(id);
	}

	@Override
	public void actualizarRespuesta(Respuesta respuesta) {
		
		respuestaRepository.save(respuesta);
		
	}

	@Override
	public void eliminarRespuesta(Long id) {
		
		respuestaRepository.deleteById(id);
		
	}
	
	

}
