package com.prueba.tigerTeam.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tigerTeam.entity.Examen;
import com.prueba.tigerTeam.entity.PreguntaExamen;
import com.prueba.tigerTeam.iService.PreguntaService;
import com.prueba.tigerTeam.repository.ExamenRepository;
import com.prueba.tigerTeam.repository.PreguntaExamenRepository;


@Service
public class PreguntaServiceImpl implements PreguntaService {
	
	
	@Autowired
	private PreguntaExamenRepository preguntaExamenRepository;
	
	
	@Autowired
	private ExamenRepository examenRepository;

	@Override
	public PreguntaExamen crearPreguntaExamen(Long id_examen, String texto, Integer score) {
		
		Examen examen = examenRepository.findById(id_examen)
				.orElseThrow(() -> new IllegalArgumentException("Examen no encontrado: " + id_examen));
		
		PreguntaExamen preguntaExamen = new PreguntaExamen();
		
		preguntaExamen.setExam(examen);
		preguntaExamen.setTexto(texto);
		preguntaExamen.setScore(score);
		
		return preguntaExamenRepository.save(preguntaExamen);
	}

	@Override
	public List<PreguntaExamen> obtenerTodasLasPreguntasDeExamenes() {
		
		return preguntaExamenRepository.findAll();
	}

	@Override
	public Optional<PreguntaExamen> obtenerPreguntaExamenPorId(Long id) {
		
		return preguntaExamenRepository.findById(id);
	}


	@Override
	public void eliminarPreguntarExamen(Long id) {
		
		preguntaExamenRepository.deleteById(id);
		
	}
	
	

}
