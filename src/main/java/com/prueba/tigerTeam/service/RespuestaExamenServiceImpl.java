package com.prueba.tigerTeam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tigerTeam.entity.AsignacionExamen;
import com.prueba.tigerTeam.entity.PreguntaExamen;
import com.prueba.tigerTeam.entity.Respuesta;
import com.prueba.tigerTeam.entity.RespuestaExamen;
import com.prueba.tigerTeam.iService.RespuestaExamenService;
import com.prueba.tigerTeam.repository.AsignacionExamenRepository;
import com.prueba.tigerTeam.repository.PreguntaExamenRepository;
import com.prueba.tigerTeam.repository.RespuestaExamenRepository;
import com.prueba.tigerTeam.repository.RespuestaRepository;

@Service
public class RespuestaExamenServiceImpl implements RespuestaExamenService {

	
	@Autowired
	private RespuestaExamenRepository respuestaExamenRepository;
	
	@Autowired
	private AsignacionExamenRepository asignacionExamenRepository;
	
	@Autowired
	private PreguntaExamenRepository preguntaExamenRepository;
	
	@Autowired
	private RespuestaRepository respuestaRepository;
	
	@Override
	public RespuestaExamen crearRespuestaExamen(Long id_asignacion_examen, Long id_pregunta_examen, Long id_respuesta) {
		
		
		AsignacionExamen asignacionExamen = asignacionExamenRepository.findById(id_asignacion_examen)
				.orElseThrow(() -> new IllegalArgumentException("No se encontro la asignacion de examen con id: " + id_asignacion_examen));
		
		PreguntaExamen preguntaExamen = preguntaExamenRepository.findById(id_pregunta_examen)
				.orElseThrow(() -> new IllegalArgumentException("No se encontro la pregunta de examen con id: " + id_pregunta_examen));
		
		Respuesta respuesta = respuestaRepository.findById(id_respuesta)
				.orElseThrow(() -> new IllegalArgumentException("No se encontro la respuesta de examen con id: " + id_respuesta));
		
		RespuestaExamen respuestaExamen = new RespuestaExamen();
		
		respuestaExamen.setAsignacionExamen(asignacionExamen);
		respuestaExamen.setPreguntaExamen(preguntaExamen);
		respuestaExamen.setRespuesta(respuesta);

		return respuestaExamenRepository.save(respuestaExamen);
	}

	@Override
	public List<RespuestaExamen> obtenerTodasLasRespuestasDeExamen() {
		
		
		return respuestaExamenRepository.findAll();
	}

	@Override
	public Optional<RespuestaExamen> obtenerRespuestaExamenPorId(Long id) {

		return respuestaExamenRepository.findById(id);
	}

	@Override
	public void actualizarRespuestaExamen(RespuestaExamen respuestaExamen) {
		
		respuestaExamenRepository.save(respuestaExamen);
		
	}

	@Override
	public void eliminarRespuestaExamen(Long id) {
		
		respuestaExamenRepository.deleteById(id);
		
	}

	@Override
	public List<RespuestaExamen> obtenerRespuestaExamenPorEstudianteyMateria(Long id_estudiante, Long id_examen) {
		

		return respuestaExamenRepository.findByAsignacionExamen_Estudiante_IdAndAsignacionExamen_Examen_Id(id_estudiante, id_examen);
	}
	
	
	@Override
	public List<Map.Entry<String, Object>> obtenerCalificacionPorEstudiantePorMateria(Long id_estudiante, Long id_examen){
		
		Map<String, Object> datosCalificacion = new HashMap();
		List<RespuestaExamen> resExamen = respuestaExamenRepository.findByAsignacionExamen_Estudiante_IdAndAsignacionExamen_Examen_Id(id_estudiante, id_examen);
		
		int puntajeTotal = 0;
		int score = 0;
		
		for(RespuestaExamen respuesta : resExamen) {
			score = respuesta.getAsignacionExamen().getExamen().getTotalScore();
			if(respuesta.getRespuesta().isCorrecto()) {
				puntajeTotal += respuesta.getPreguntaExamen().getScore();
			}
			
		}
		
		if(puntajeTotal < 0) {
			puntajeTotal = 0;
		}
		
		double calificacion = ((double) puntajeTotal / score) * 100;
		
		for(RespuestaExamen respuesta : resExamen) {
			datosCalificacion.put("Nombre", respuesta.getAsignacionExamen().getEstudiante().getNombre());
			datosCalificacion.put("Materia", respuesta.getAsignacionExamen().getExamen().getNombre());
			datosCalificacion.put("Calificacion", calificacion);
			
		}
		
		List<Map.Entry<String, Object>> lista = new ArrayList<>();
	    for (Map.Entry<String, Object> entrada : datosCalificacion.entrySet()) {
	        lista.add(entrada);
	    }
		
		return lista;
	}

}
