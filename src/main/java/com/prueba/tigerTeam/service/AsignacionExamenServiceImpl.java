package com.prueba.tigerTeam.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tigerTeam.entity.AsignacionExamen;
import com.prueba.tigerTeam.entity.Estudiante;
import com.prueba.tigerTeam.entity.Examen;
import com.prueba.tigerTeam.iService.AsignacionExamenService;
import com.prueba.tigerTeam.repository.AsignacionExamenRepository;

@Service
public class AsignacionExamenServiceImpl implements AsignacionExamenService {
	
	
	@Autowired
	private AsignacionExamenRepository asignacionExamenRepository;

	@Override
	public AsignacionExamen crearAsignacionExamen(Long id_examen, Long id_estudiante, LocalDateTime fechaExamen) {
		
		AsignacionExamen asignacionEx = new AsignacionExamen();
		
		Examen examen = new Examen();
		examen.setId(id_examen);
		
		asignacionEx.setExamen(examen);
		
		Estudiante estudiante = new Estudiante();
		estudiante.setId(id_estudiante);
		
		asignacionEx.setEstudiante(estudiante);
		
		asignacionEx.setFechaExamen(fechaExamen);

		return asignacionExamenRepository.save(asignacionEx);
		
	}

	@Override
	public List<AsignacionExamen> obtenerTodosLosExamenesAsignados() {

		return asignacionExamenRepository.findAll();
	}

	@Override
	public Optional<AsignacionExamen> obtenerExamenesAsignadorPorEstudiante(Long id_estudiante) {

		return asignacionExamenRepository.findById(id_estudiante);
	}

	@Override
	public Optional<AsignacionExamen> obtenerExamenesAsignadorPorExamen(Long id_examen) {

		return asignacionExamenRepository.findById(id_examen);
	}

	@Override
	public Optional<AsignacionExamen> obtenerExamenesAsignadorPorId(Long id) {

		return asignacionExamenRepository.findById(id);
	}

	@Override
	public void actualizarAsignacionExamen(AsignacionExamen asignacionExamen) {
		
		asignacionExamenRepository.save(asignacionExamen);
		
	}

	@Override
	public void eliminarAsignacionExamen(Long id) {
		
		asignacionExamenRepository.deleteById(id);
		
	}

}
