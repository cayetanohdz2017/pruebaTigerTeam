package com.prueba.tigerTeam.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tigerTeam.entity.Estudiante;
import com.prueba.tigerTeam.iService.EstudianteService;
import com.prueba.tigerTeam.repository.EstudianteRepository;

@Service
public class EstudianteServiceImpl implements EstudianteService {
	
	
	@Autowired
	private EstudianteRepository estudianteRepository;

	@Override
	public Estudiante crearEstudiante(Estudiante estudiante) {

		return estudianteRepository.save(estudiante);
	}

	@Override
	public List<Estudiante> obtenerTodosLosEstudiantes() {

		return estudianteRepository.findAll();
	}

	@Override
	public Optional<Estudiante> obtenerEstudiantePorId(Long id) {

		return estudianteRepository.findById(id);
	}

	@Override
	public void eliminarEstudiantePorId(Long id) {
		

		estudianteRepository.deleteById(id);
		
	}

}
