package com.prueba.tigerTeam.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tigerTeam.entity.AsignacionExamen;
import com.prueba.tigerTeam.entity.Estudiante;
import com.prueba.tigerTeam.entity.Examen;
import com.prueba.tigerTeam.iService.ExamenService;
import com.prueba.tigerTeam.repository.AsignacionExamenRepository;
import com.prueba.tigerTeam.repository.EstudianteRepository;
import com.prueba.tigerTeam.repository.ExamenRepository;


@Service
public class ExamenServiceImp implements ExamenService {
	
	@Autowired
	private ExamenRepository examenRepository;
	
	@Autowired
	private EstudianteRepository estudianteRepository;
	
	@Autowired
	private AsignacionExamenRepository asignacionExamenRepository;

	
	@Override
	public Examen crearExamen(Examen examen) {

		return examenRepository.save(examen);
	}

	@Override
	public List<Examen> obtenerTodosLosExamenes() {

		return examenRepository.findAll();
	}

	@Override
	public Optional<Examen> obtenerExamenPorId(Long id) {

		return examenRepository.findById(id);
	}

	@Override
	public void eliminarExamenPorId(Long id) {
		
		examenRepository.deleteById(id);
		
	}

	@Override
	public void asignarExamenAEstudiante(Long id_examen, Long id_estudiante, LocalDateTime fechaPresentacion) {
		
		Optional<Examen> examen = obtenerExamenPorId(id_examen);
		
		Optional<Estudiante> estudiante = estudianteRepository.findById(id_estudiante);
		
		AsignacionExamen asignacionExamen = new AsignacionExamen();
		
		asignacionExamen.setExamen(examen.get());
		asignacionExamen.setEstudiante(estudiante.get());
		asignacionExamen.setFechaExamen(fechaPresentacion);
		
		asignacionExamenRepository.save(asignacionExamen);
		
		
		
	}

}
