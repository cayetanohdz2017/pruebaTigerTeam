package com.prueba.tigerTeam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TigerTeamApplication {

	public static void main(String[] args) {
		SpringApplication.run(TigerTeamApplication.class, args);
	}

}
